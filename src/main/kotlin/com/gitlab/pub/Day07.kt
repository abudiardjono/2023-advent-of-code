package com.gitlab.pub

fun main(args: Array<String>) {
    Day07("day07.txt").solveAndPrint()
}

/**
 * Any sort of "card game" type question with hands need to be solved with some sort of hand finding state machine(s).
 * For every hand, we read in each "card" and decide what hand types we can eliminate with the new addition. We don't
 * care which cards we actually have, just how many duplicates we may or may not have of each type - so we just use
 * a map to track the number of cards of each type per hand.
 *
 * We have a hand finder for every type of hand. They must all run simultaneously. As we read in cards from the hand,
 * the finders will update their state to determine if the hand type is still possible based on the number of cards
 * we've already read, and the number of cards left to be read. For example, if you are reading card number 5 and
 * you don't already have a pair, all hand types from TWO_PAIR up are invalid. The 5th card you are reading could still
 * give you ONE_PAIR from HIGH though.
 *
 * For Part 1, we figure out each hand's type, put each hand into lists with others of the same type, sort the list
 * based on our custom card to card comparator, and then traverse through each list in order from low type to get
 * the rank used for multiplication.
 *
 * For part 2, there is just an extra step and one modification to our finders before we sort the lists. First, we
 * tell all our finders to ignore 'J' cards - treat them like empty cards that take up room but give us nothing. Then,
 * when we have our preliminary hand type (the hand type if we ignored Js), we upgrade the hand based on the type
 * and number of Js - the best upgrade will always be to increase the count of the highest card count by the number
 * of available Js.
 */
class Day07(input: String) : Day(input) {
    private val handTypesMap = mutableMapOf(
        Type.HIGH to mutableListOf<Pair<String, Int>>(),
        Type.ONE_PAIR to mutableListOf(),
        Type.TWO_PAIR to mutableListOf(),
        Type.THREE_KIND to mutableListOf(),
        Type.FULL_HOUSE to mutableListOf(),
        Type.FOUR_KIND to mutableListOf(),
        Type.FIVE_KIND to mutableListOf(),
    )
    private val camelHandFinder = CamelHandFinder()
    override fun solvePartOne(inputLines: List<String>): Number {
        val labelMap = mapOf(
            'A' to 1, 'K' to 2, 'Q' to 3, 'J' to 4, 'T' to 5, '9' to 6, '8' to 7,
            '7' to 8, '6' to 9, '5' to 10, '4' to 11, '3' to 12, '2' to 13
        )

        val handComparator = Comparator<Pair<String, Int>> { a, b ->
            for (i in 0..a.first.lastIndex) {
                val charA = a.first[i]
                val charB = b.first[i]
                if (labelMap[charA]!! < labelMap[charB]!!) return@Comparator 1
                else if (labelMap[charA]!! > labelMap[charB]!!) return@Comparator -1
            }
            return@Comparator 0
        }

        return solve(inputLines, handComparator, 1)
    }

    override fun solvePartTwo(inputLines: List<String>): Number {
        val labelMap = mapOf(
            'A' to 1, 'K' to 2, 'Q' to 3, 'T' to 4, '9' to 5, '8' to 6, '7' to 7,
            '6' to 8, '5' to 9, '4' to 10, '3' to 11, '2' to 12, 'J' to 13,
        )
        val handComparator = Comparator<Pair<String, Int>> { a, b ->
            for (i in 0..a.first.lastIndex) {
                val charA = a.first[i]
                val charB = b.first[i]
                if (labelMap[charA]!! < labelMap[charB]!!) return@Comparator 1
                else if (labelMap[charA]!! > labelMap[charB]!!) return@Comparator -1
            }
            return@Comparator 0
        }

        return solve(inputLines, handComparator, 2)
    }

    private fun solve(inputLines: List<String>, comparator: Comparator<Pair<String, Int>>, part: Int): Number {
        handTypesMap.forEach { it.value.clear() }
        inputLines.forEach { line ->
            val splitLine = line.split("\\s+".toRegex())
            val hand = splitLine[0]
            val bid = splitLine[1]
            val handType = findHand(hand, part)
            handTypesMap[handType]!!.add(hand to bid.toInt())
        }

        var rank = 1
        var sum = 0
        handTypesMap.forEach { map ->
            map.value.sortedWith(comparator).forEach {
                sum += it.second * rank
                rank++
            }
        }

        return sum
    }

    private fun findHand(hand: String, part: Int): Type {
        camelHandFinder.reset()
        hand.forEach { camelHandFinder.find(it, part) }
        var type = camelHandFinder.getCurrentValidTypes()[0] // as long as the hands are well-formed this is fine

        if (part == 2 && hand.contains('J')) {
            type = transformJokerHand(hand, type)
        }

        return type
    }

    private fun transformJokerHand(hand: String, ogType: Type): Type {
        val numJ = hand.filter { it == 'J' }.length
        when (ogType) {
            Type.HIGH -> {
                when (numJ) {
                    1 -> return Type.ONE_PAIR
                    2 -> return Type.THREE_KIND
                    3 -> return Type.FOUR_KIND
                    4 -> return Type.FIVE_KIND
                    5 -> return Type.FIVE_KIND // special case for JJJJJ
                }
            }

            Type.ONE_PAIR -> {
                when (numJ) {
                    1 -> return Type.THREE_KIND
                    2 -> return Type.FOUR_KIND
                    3 -> return Type.FIVE_KIND
                }
            }

            Type.TWO_PAIR -> {
                when (numJ) {
                    1 -> return Type.FULL_HOUSE
                }
            }

            Type.THREE_KIND -> {
                when (numJ) {
                    1 -> return Type.FOUR_KIND
                    2 -> return Type.FIVE_KIND
                }
            }

            Type.FULL_HOUSE -> {}
            Type.FOUR_KIND -> return Type.FIVE_KIND
            Type.FIVE_KIND -> {}
        }
        return ogType
    }

    enum class Type {
        HIGH,
        ONE_PAIR,
        TWO_PAIR,
        THREE_KIND,
        FULL_HOUSE,
        FOUR_KIND,
        FIVE_KIND
    }

    class CamelHandFinder {
        private val finders = listOf(
            HighHandFinder(),
            PairHandFinder(),
            TwoPairHandFinder(),
            ThreeKindHandFinder(),
            FullHouseHandFinder(),
            FourKindHandFinder(),
            FiveKindHandFinder()
        )

        private var charMap = mutableMapOf<Char, Int>()
        private var charCount = 0

        fun find(c: Char, part: Int) {
            charCount++
            if (charMap[c] == null) charMap[c] = 1
            else {
                charMap[c] = charMap[c]!! + 1
            }

            var count = charMap[c]!!
            if (part == 2 && c == 'J') {
                count = 0
            }

            finders.forEach { it.find(count, charCount) }
        }

        fun getCurrentValidTypes(): List<Type> {
            return finders.filter { it.state != -1 }.map { it.type }
        }

        fun reset() {
            charCount = 0
            charMap.clear()
            finders.forEach { it.reset() }
        }
    }

    abstract class HandFinder(val type: Type, var state: Int = 0) {
        fun reset() {
            state = 0
        }

        abstract fun find(count: Int, totalCount: Int): Boolean
    }

    class HighHandFinder : HandFinder(Type.HIGH) {
        override fun find(count: Int, totalCount: Int): Boolean {
            when (state) {
                0 -> if (count > 1) state = -1
            }
            return state != -1
        }
    }

    class PairHandFinder : HandFinder(Type.ONE_PAIR) {
        override fun find(count: Int, totalCount: Int): Boolean {
            when (state) {
                0 -> if (count == 2) state = 1
                1 -> if (count > 1) state = -1
            }
            if (state < 1 && totalCount == 5) state = -1
            return state != -1
        }
    }

    class TwoPairHandFinder : HandFinder(Type.TWO_PAIR) {
        override fun find(count: Int, totalCount: Int): Boolean {
            when (state) {
                0 -> {
                    if (count == 2) state = 1
                }

                1 -> {
                    if (count == 2) state = 2
                    if (count == 3) state = -1
                }

                2 -> if (count == 3) state = -1
            }
            if (state < 2 && totalCount == 5) state = -1
            return state != -1
        }
    }

    class ThreeKindHandFinder : HandFinder(Type.THREE_KIND) {
        override fun find(count: Int, totalCount: Int): Boolean {
            when (state) {
                0 -> {
                    if (count == 2) state = 1
                }

                1 -> {
                    if (count == 2) state = -1
                    if (count == 3) state = 2
                }

                2 -> if (count >= 2) state = -1
            }
            if (state < 2 && totalCount == 5) state = -1
            return state != -1
        }
    }

    class FullHouseHandFinder : HandFinder(Type.FULL_HOUSE) {
        override fun find(count: Int, totalCount: Int): Boolean {
            when (state) {
                0 -> if (count == 2) state = 1
                1 -> if (count == 2 || count == 3) state = 2
                2 -> if (count == 2 || count == 3) state = 3
            }
            if (state < 3 && totalCount == 5) state = -1
            return state != -1
        }
    }

    class FourKindHandFinder : HandFinder(Type.FOUR_KIND) {
        override fun find(count: Int, totalCount: Int): Boolean {
            when (state) {
                0 -> if (count == 4) state = 1
                1 -> if (count == 5) state = -1
            }
            if (state < 1 && totalCount == 5) state = -1
            return state != -1
        }
    }

    class FiveKindHandFinder : HandFinder(Type.FIVE_KIND) {
        override fun find(count: Int, totalCount: Int): Boolean {
            when (state) {
                0 -> if (count == 5) state = 1
            }
            if (state < 1 && totalCount == 5) state = -1
            return state != -1
        }
    }

}