package com.gitlab.pub

fun main(args: Array<String>) {
    DayTwo("day02.txt").solveAndPrint()
}

/**
 * Parsing simulator part 2. We speed through by feeding each char of the file into our parser state
 * machine. Technically there are 3 states - in the  header, in the draw, and then in the cube selection.
 * But we don't actually need to read the full word for each color, just the first letter, so we add some skip states
 * where we don't do anything.
 *
 * The minimum information we need to know for each cube selection is the number and the first letter of the color,
 * ex 3 g(reen), 2 r(ed). So once we get what we want we just skip the rest. For part 1 we just need to find
 * the first failure in the draw, then we can skip the rest of the draw. For part 2, we have to read each
 * cube selection, but we skip over the spaces and chars that are not the first letter of the color.
 */
class DayTwo(input1: String) : Day(input1) {

    private val maxMap = mapOf(
        'r' to 12,
        'g' to 13,
        'b' to 14
    )

    enum class State {
        IN_HEADER,
        IN_DRAW,
        SKIP_DRAW,
        IN_CUBE,
        SKIP_CUBE,
    }

    override fun solvePartOne(inputLines: List<String>): Int {
        return solve(inputLines, 1)
    }

    override fun solvePartTwo(inputLines: List<String>): Int {
        return solve(inputLines, 2)
    }

    private fun solve(inputLines: List<String>, part: Int): Int {
        var sum = 0
        var index = 0
        for (line in inputLines) {
            var state = State.IN_HEADER
            var currentNumber = ""
            var valid = true
            var power = 1
            val minMap = mutableMapOf(
                'r' to -1,
                'g' to -1,
                'b' to -1
            )

            line.forEach {
                when (state) {
                    State.IN_HEADER -> if (it == ':') state = State.IN_DRAW
                    State.IN_DRAW -> {
                        if (it.isDigit()) {
                            currentNumber += it
                            state = State.IN_CUBE
                        }
                    }

                    State.IN_CUBE -> {
                        if (it.isDigit()) {
                            currentNumber += it
                        }
                        if (currentNumber.isNotEmpty() && maxMap[it] != null) {
                            if (part == 1) {
                                if (currentNumber.toInt() > maxMap[it]!!) {
                                    valid = false
                                    state = State.SKIP_DRAW
                                }
                            } else {
                                val min = minMap[it]!!
                                if (min == -1 || currentNumber.toInt() > min) {
                                    minMap[it] = currentNumber.toInt()
                                    state = State.SKIP_CUBE
                                }
                            }
                            currentNumber = ""
                        }
                    }

                    State.SKIP_CUBE -> {
                        if (it == ',') state = State.IN_CUBE
                        if (it == ';') state = State.IN_DRAW
                    }

                    State.SKIP_DRAW -> if (it == ';') state = State.IN_DRAW
                }
            }

            if (part == 1) {
                if (valid) sum += index + 1
            } else {
                minMap.values.forEach { if (it != -1) power *= it }
                sum += power
            }
            index++
        }
        return sum
    }
}