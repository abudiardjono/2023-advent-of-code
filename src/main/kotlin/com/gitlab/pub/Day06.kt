package com.gitlab.pub

fun main(args: Array<String>) {
    Day06("day06.txt").solveAndPrint()
}

/**
 * We don't actually have to count every single way to win the race. We only need to know two ways - the
 * minimum amount of time we have to hold the button, and the maximum amount of time we can hold it. Any time
 * in between these two are all valid, so the amount of times we can win equals the difference between the
 * maximum amount of time and the minimum, plus one (because it is inclusive). The distance d you can travel within
 * the allotted time t while holding the button for x amount of ms is d = (x * (t-x)).
 *
 * We can use this to solve both Parts 1 and 2 - just change the calculations at the end of each race.
 */
class Day06(input: String) : Day(input) {

    private val timeRegex = "Time:\\s+(?<times>[\\s\\d]+)".toRegex()
    private val distanceRegex = "Distance:\\s+(?<distances>[\\s\\d]+)".toRegex()

    private val races = mutableListOf<Pair<Long, Long>>()

    override fun solvePartOne(inputLines: List<String>): Number {
        parseInput(inputLines, 1)
        var solution = 1L
        races.forEach { (time, distance) ->
            val minTime = findMinTime(time, distance)
            val maxTime = findMaxTime(time, distance)
            solution *= (maxTime - minTime) + 1
        }
        return solution
    }

    override fun solvePartTwo(inputLines: List<String>): Number {
        parseInput(inputLines, 2)
        var solution = 1L
        races.forEach { (time, distance) ->
            val minTime = findMinTime(time, distance)
            val maxTime = findMaxTime(time, distance)
            solution = (maxTime - minTime) + 1
        }
        return solution
    }

    private fun findMinTime(time: Long, distance: Long): Long {
        for (t in 0..time) {
            if (t * (time - t) > distance) return t
        }
        return 0
    }

    private fun findMaxTime(time: Long, distance: Long): Long {
        for (t in time downTo 0) {
            if (t * (time - t) > distance) return t
        }
        return 0
    }

    private fun parseInput(inputLines: List<String>, part: Int) {
        races.clear()
        lateinit var times: List<Long>
        lateinit var distances: List<Long>
        inputLines.forEach { line ->
            if (timeRegex.matches(line)) {
                val timesString = timeRegex.find(line)?.groups?.get("times")?.value!!
                times = if (part == 1) {
                    timesString.split("\\s+".toRegex()).map { it.toLong() }
                } else {
                    listOf(timesString.filter { it.isDigit() }.toLong())
                }
            } else if (distanceRegex.matches(line)) {
                val distancesString = distanceRegex.find(line)?.groups?.get("distances")?.value!!
                distances = if (part == 1) {
                    distancesString.split("\\s+".toRegex()).map { it.toLong() }
                } else {
                    listOf(distancesString.filter { it.isDigit() }.toLong())
                }
            }
        }
        for ((index, time) in times.withIndex()) {
            races.add(time to distances[index])
        }
    }
}