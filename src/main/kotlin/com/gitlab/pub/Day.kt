package com.gitlab.pub

import java.util.*

/**
 * @author Proprietary information subject to the terms of a Non-Disclosure Agreement
 */
abstract class Day(inputPath: String) {

    private val inputLines = object {}.javaClass.getResourceAsStream("/$inputPath")?.bufferedReader()?.readLines()

    fun solveAndPrint() {
        inputLines?.let {
            val partOneStartTime = System.currentTimeMillis()
            val partOneStartDate = Date(partOneStartTime)
            println("Starting Part 1...$partOneStartDate")
            val partOneSolution = solvePartOne(it)
            val partOneEndTime = System.currentTimeMillis()
            val partOneTotalTimeMs = partOneEndTime - partOneStartTime
            println("Part 1 Solution: $partOneSolution finished in $partOneTotalTimeMs ms")

            val partTwoStartTime = System.currentTimeMillis()
            val partTwoStartDate = Date(partOneStartTime)
            println("Starting Part 2...$partTwoStartDate")
            val partTwoSolution = solvePartTwo(it)
            val partTwoEndTime = System.currentTimeMillis()
            val partTwoTotalTimeMs = partTwoEndTime - partTwoStartTime
            println("Part 2 Solution: $partTwoSolution finished in $partTwoTotalTimeMs ms")
        }
    }

    abstract fun solvePartOne(inputLines: List<String>): Number
    abstract fun solvePartTwo(inputLines: List<String>): Number
}