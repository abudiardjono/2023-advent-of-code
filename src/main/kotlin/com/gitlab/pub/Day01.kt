package com.gitlab.pub

fun main(args: Array<String>) {
    DayOne("day01.txt").solveAndPrint()
}

/**
 * Go through each character in every line and feed it into our number state machine. If it's a digit, we'll take it,
 * if it's a letter, we have to see if it is part of one of the 9 keywords we are looking for. We can do this by creating
 * state machines for each keyword that run independently. Every time we encounter a character, we update the state of all
 * the machines and see if any have completed. If it has completed, then we've found a number.
 *
 * We only need to read the string until we hit the first number going forwards and backwards. When we go backwards just
 * reverse the states on the machines.
 */
class DayOne(input: String) : Day(input) {
    private val findersMap = mapOf(
        StringFinder(target = "one") to "1",
        StringFinder(target = "two") to "2",
        StringFinder(target = "three") to "3",
        StringFinder(target = "four") to "4",
        StringFinder(target = "five") to "5",
        StringFinder(target = "six") to "6",
        StringFinder(target = "seven") to "7",
        StringFinder(target = "eight") to "8",
        StringFinder(target = "nine") to "9",
    )

    override fun solvePartOne(inputLines: List<String>): Int {
        var sum = 0
        // fun but slow
        inputLines.forEach { line ->
            line.filter { it.isDigit() }.let {
                sum += if (it.length == 1) {
                    (it + it).toInt()
                } else {
                    it.removeRange(IntRange(1, it.lastIndex - 1)).toInt()
                }
            }
        }
        return sum
    }

    override fun solvePartTwo(inputLines: List<String>): Int {
        var solution = 0
        inputLines.forEach { solution += getCalibrationValueFromLine(it) }
        return solution
    }

    private fun getCalibrationValueFromLine(string: String): Int {
        val firstDigit: String? = findFirstDigit(string, reverse = false)
        val secondDigit: String? = findFirstDigit(string, reverse = true)

        var number = "0"
        // even if there is only one number, neither the first nor second digit will be null
        if (firstDigit != null) {
            number = firstDigit + secondDigit
        }
        return number.toInt()
    }

    private fun findFirstDigit(input: String, reverse: Boolean): String? {
        val string = if (reverse) input.reversed() else input
        var digit: String? = null
        for (char in string) {
            if (char.isDigit()) {
                digit = char.toString()
            } else {
                findersMap.keys.forEach {
                    // no two finders will ever finish at the same time, but we have to call find on every finder
                    digit = if (it.find(char, reverse)) findersMap[it] else digit
                }
            }
            if (digit != null) break
        }
        findersMap.keys.forEach { it.reset() }
        return digit
    }

    private class StringFinder(private val target: String) {
        private var currentState = 0
        fun find(input: Char, reverse: Boolean): Boolean {
            var index = if (reverse) target.length - 1 - currentState else currentState
            if (input == target[index]) {
                currentState++
            } else {
                // for every input it could be the next char in the sequence or the first
                currentState = 0
                index = if (reverse) target.length - 1 - currentState else currentState
                if (input == target[index]) {
                    currentState++
                }
            }

            if (currentState == target.length) {
                currentState = 0
                return true
            }

            return false
        }

        fun reset() {
            currentState = 0
        }
    }
}
