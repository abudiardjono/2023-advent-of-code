package com.gitlab.pub

fun main(args: Array<String>) {
    Day10("day10.txt").solveAndPrint()
}

/**
 * Part 1 is just following the path. Any tile x, only has one entrance or exit, so in the beginning we build a map
 * of nodes and give it two paths based on the character. That way, when we traverse it we can just follow the line of
 * non-null nodes.
 *
 * For Part 2 it took way too long for me to figure out what counts as a vertical wall. The logic is that for any row -
 * in order for some tile x be "IN" the shape, it must be to the right of an odd number of vertical walls.
 * The tricky part was realizing two things:
 *
 * "F7 or F-*7" and "LJ or L-*J" counts as 2 vertical walls
 * "FJ or F-*J" and "L7 or L-*7" counts as 1 vertical wall since together, they make an elbow zzzzzzzz
 */
class Day10(input: String) : Day(input) {

    private val map = mutableListOf<MutableList<Node>>()
    private val hasDown = listOf('7', 'F', '|', 'S')
    private val hasUp = listOf('J', 'L', '|', 'S')
    private val hasLeft = listOf('7', 'J', '-', 'S')
    private val hasRight = listOf('F', 'L', '-', 'S')
    private var startingNode: Node? = null

    override fun solvePartOne(inputLines: List<String>): Number {
        buildMap(inputLines)

        // find the loop
        var currentNode = startingNode!!
        var len = 0
        var from = Direction.North
        while (currentNode.char != 'S' || len == 0) {
            val next = findNext(currentNode, from)
            currentNode = next.first
            from = next.second
            len++
        }

        return len / 2
    }


    override fun solvePartTwo(inputLines: List<String>): Number {
        buildMap(inputLines)

        // find the loop
        var currentNode = startingNode!!
        var len = 0
        var from = Direction.North
        while (currentNode.char != 'S' || len == 0) {
            currentNode.state = LoopState.MEMBER
            val next = findNext(currentNode, from)
            from = next.second
            currentNode = next.first
            len++
        }

        // figure out what shape starting node is then replace it
        startingNode!!.char = solveMysteryNode(startingNode!!)

        var numIn = 0
        map.forEach { row ->
            var numWalls = 0
            var numL = 0
            var numF = 0
            row.forEach { node ->
                if (node.state == LoopState.MEMBER) {
                    when (node.char) {
                        '|' -> numWalls++
                        'F' -> numF++
                        'L' -> numL++
                        'J' -> {
                            if (numF > 0) {
                                numWalls++
                                numF--
                            }
                            if (numL > 0) numL--
                        }

                        '7' -> {
                            if (numL > 0) {
                                numWalls++
                                numL--
                            }
                            if (numF > 0) numF--
                        }
                    }
                } else {
                    if (numWalls % 2 != 0) {
                        node.state = LoopState.IN____
                        numIn++
                    }
                }
            }
        }

        return numIn
    }

    private fun buildMap(inputLines: List<String>) {
        map.clear()
        startingNode = null
        inputLines.mapIndexed { lineIndex, line ->
            map.add(mutableListOf())
            val hasUpIndex = lineIndex != 0

            line.mapIndexed { charIndex, char ->
                val hasLeftIndex = charIndex != 0

                // we only set the left and up nodes since those exist
                val node = Node(char)
                if (hasUpIndex) {
                    val up = map[lineIndex - 1][charIndex]
                    if (hasDown.contains(up.char) && hasUp.contains(node.char)) {
                        node.up = lineIndex - 1 to charIndex
                        up.down = lineIndex to charIndex
                    }
                }
                if (hasLeftIndex) {
                    val left = map[lineIndex][charIndex - 1]
                    if (hasRight.contains(left.char) && hasLeft.contains(node.char)) {
                        node.left = lineIndex to charIndex - 1
                        left.right = lineIndex to charIndex
                    }
                }

                map[lineIndex].add(node)

                if (node.char == 'S') startingNode = node
            }
        }
    }

    private fun findNext(node: Node, from: Direction): Pair<Node, Direction> {
        return Direction.values().filter { it != from }.mapNotNull { checkDirection(node, it) }[0]
    }

    private fun checkDirection(node: Node, direction: Direction): Pair<Node, Direction>? {
        return when (direction) {
            Direction.North -> if (node.up != null) map[node.up!!.first][node.up!!.second] to Direction.South else null
            Direction.South -> if (node.down != null) map[node.down!!.first][node.down!!.second] to Direction.North else null
            Direction.East -> if (node.right != null) map[node.right!!.first][node.right!!.second] to Direction.West else null
            Direction.West -> if (node.left != null) map[node.left!!.first][node.left!!.second] to Direction.East else null
        }
    }

    private fun solveMysteryNode(node: Node): Char {
        val hasNorth = checkDirection(node, Direction.North) != null
        val hasSouth = checkDirection(node, Direction.South) != null
        val hasEast = checkDirection(node, Direction.East) != null
        val hasWest = checkDirection(node, Direction.West) != null

        if (hasNorth && hasSouth) return '|'
        if (hasNorth && hasEast) return 'L'
        if (hasNorth && hasWest) return 'J'
        if (hasSouth && hasEast) return 'F'
        if (hasSouth && hasWest) return '7'
        if (hasEast && hasWest) return '-'

        return '?'
    }

    enum class LoopState {
        MEMBER,
        IN____,
        OUT___
    }

    enum class Direction {
        North,
        South,
        East,
        West
    }

    data class Node(
        var char: Char,
        var distance: Int? = null,
        var state: LoopState = LoopState.OUT___,
        var up: Pair<Int, Int>? = null,
        var down: Pair<Int, Int>? = null,
        var left: Pair<Int, Int>? = null,
        var right: Pair<Int, Int>? = null
    )
}
