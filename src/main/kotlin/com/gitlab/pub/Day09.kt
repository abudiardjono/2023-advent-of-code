package com.gitlab.pub

fun main(args: Array<String>) {
    Day09("day09.txt").solveAndPrint()
}

/**
 * Nothing special to note here, just eat memory with your list of lists and do the thing. We build our
 * gigantic map of values then use either the last or first value for our calculations based on whether we
 * are solving for Part 1 or 2
 */
class Day09(input: String) : Day(input) {
    override fun solvePartOne(inputLines: List<String>): Number {
        var sum = 0
        inputLines.forEach { line ->
            val history = line.split("\\s+".toRegex()).map { it.toInt() }.toMutableList()
            sum += extrapolate(history, true)
        }

        return sum
    }

    override fun solvePartTwo(inputLines: List<String>): Number {
        var sum = 0
        inputLines.forEach { line ->
            val history = line.split("\\s+".toRegex()).map { it.toInt() }.toMutableList()
            sum += extrapolate(history, false)
        }

        return sum
    }

    private fun extrapolate(history: MutableList<Int>, future: Boolean): Int {
        val container = mutableListOf<MutableList<Int>>()
        var currentList = history
        container.add(currentList)
        while (!currentList.all { it == 0 }) {
            val newList = mutableListOf<Int>()
            currentList.mapIndexed { index, i ->
                if (index > 0) {
                    newList.add(i - currentList[index - 1])
                }
            }
            container.add(newList)
            currentList = newList
        }

        val reversedContainer = container.reversed()
        reversedContainer.forEachIndexed { index, list ->
            if (index > 0) {
                val prevList = reversedContainer[index - 1]
                if (future) {
                    list.add(list.last() + prevList.last())
                } else {
                    list.add(0, list.first() - prevList.first())
                }
            }
        }

        return if (future) container.first().last() else container.first().first()
    }
}