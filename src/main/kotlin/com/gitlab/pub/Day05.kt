package com.gitlab.pub

fun main(args: Array<String>) {
    Day05("day05.txt").solveAndPrint()
}

/**
 * For Part 1, at it most basic - the idea is to take some input and move it through a number of states where the
 * input to the next state is the output of the current state. The janky Almanac conveniently lists the maps (states)
 * in the order we need, so we'll just read through each map and process each seed as we go.For each entry in the map
 * we try to transform the seed into a new value - if we get one, we mark the seed as transformed so it doesn't get
 * processed on another entry in the same map. When we encounter a new map, we'll mark all the seeds as not yet transformed.
 *
 * For Part 2 it's better to work backwards. Since we just need to know the smallest location number, if we start
 * from the smallest location number (0) then we can stop once we find the first matching seed. Is it the best? probably not.
 * But it's faster than starting with the seed ranges KEKW.
 */
class Day05(input: String) : Day(input) {
    private val seedsRegex = "seeds:\\s+(?<seedNumbers>[\\d\\s]+)".toRegex()
    private val mapNameRegex = "(?<mapName>[\\w\\S]+)\\s+map:".toRegex()
    private val mapValuesRegex = "(?<destStart>\\d+)\\s(?<srcStart>\\d+)\\s(?<len>\\d+)".toRegex()

    private val maps = mapOf(
        "seed-to-soil" to mutableListOf<Triple<Long, Long, Long>>(),
        "soil-to-fertilizer" to mutableListOf(),
        "fertilizer-to-water" to mutableListOf(),
        "water-to-light" to mutableListOf(),
        "light-to-temperature" to mutableListOf(),
        "temperature-to-humidity" to mutableListOf(),
        "humidity-to-location" to mutableListOf(),
    )

    private val seeds = mutableListOf<Long>()
    private val seedRanges = mutableListOf<LongRange>()

    override fun solvePartOne(inputLines: List<String>): Long {
        buildMaps(inputLines)
        var minLocation = -1L
        seeds.map { Holder(it, false) }.forEach { seed ->
            maps.entries.forEach { entry ->
                seed.transformed = false
                entry.value.forEach { triple ->
                    if (!seed.transformed) {
                        val transformation = transform(seed.value, triple.first, triple.second, triple.third)
                        if (transformation != seed.value) {
                            seed.value = transformation
                            seed.transformed = true
                        }
                    }
                }
            }
            if (minLocation == -1L || seed.value < minLocation) minLocation = seed.value
        }

        return minLocation
    }

    override fun solvePartTwo(inputLines: List<String>): Long {
        buildMaps(inputLines)
        var minLocation = -1L
        var location = 0L
        while (minLocation == -1L) {
            val seed = Holder(location, false)
            maps.entries.reversed().forEach { entry ->
                seed.transformed = false
                entry.value.forEach { triple ->
                    if (!seed.transformed) {
                        val transformation = transform(seed.value, triple.second, triple.first, triple.third)
                        if (transformation != seed.value) {
                            seed.value = transformation
                            seed.transformed = true
                        }
                    }
                }
            }
            seedRanges.find { it.contains(seed.value) }?.let { minLocation = location }
            seed.value = ++location
        }

        return minLocation
    }

    private fun buildMaps(inputLines: List<String>) {
        var mapName = ""
        inputLines.forEach { line ->
            if (seedsRegex.matches(line)) {
                val result = seedsRegex.find(line)!!
                val seedInputs = result.groups["seedNumbers"]?.value?.split("\\s+".toRegex())!!.map { it.toLong() }
                var holder = 0L
                seedInputs.withIndex().forEach { (index, value) ->
                    if (index % 2 == 0) {
                        holder = value
                    } else {
                        seedRanges.add(holder until holder + value)
                    }
                    seeds.add(value)
                }
            } else if (mapNameRegex.matches(line)) {
                mapName = mapNameRegex.find(line)?.groups?.get("mapName")?.value!!
            } else if (mapValuesRegex.matches(line)) {
                val result = mapValuesRegex.find(line)!!
                val destStart = result.groups["destStart"]?.value!!.toLong()
                val srcStart = result.groups["srcStart"]?.value!!.toLong()
                val len = result.groups["len"]?.value!!.toLong()
                maps[mapName]?.add(Triple(destStart, srcStart, len))
            }
        }
    }

    private fun transform(input: Long, destStart: Long, srcStart: Long, len: Long): Long {
        if (input in (srcStart..(srcStart + (len - 1)))) return destStart + (input - srcStart)
        return input
    }

    data class Holder(var value: Long, var transformed: Boolean)
}
