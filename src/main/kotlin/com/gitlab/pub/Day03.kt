package com.gitlab.pub

fun main(args: Array<String>) {
    DayThree("day03.txt").solveAndPrint()
}

/**
 * We're lazy so we're gonna be slow and loop through the full set twice, one after the other. The first time
 * we'll look for symbols and mark each box it touches (adjacent and diagonally) as a valid box. The second time
 * we'll parse the numbers as we read them, and if any of the digits are in a "valid" box, we'll add them to our sum.
 *
 * For part 2, for every digit in a valid number, we'll collect the symbol(s) affecting that digit. When the
 * number has been fully parsed, if it's valid, we'll add the number to a map of symbols to List<affected numbers>.
 * At the end we'll go through the map and add all the gear ratios of the symbols with more than 1 number.
 *
 * We could make it faster by starting the number loop while the symbol loop is running, but 2 rows behind. Since a
 * symbol on (row, column) can only affect boxes on (row-1) and (row+1) we can start processing numbers on (row-2)
 */
class DayThree(input1: String) : Day(input1) {

    private var validDigitToSymbolsMap = mutableMapOf<Pair<Int, Int>, MutableList<Pair<Int, Int>>>()
    private var symbolToPartNumbersMap = mutableMapOf<Pair<Int, Int>, MutableList<Int>>()

    override fun solvePartOne(inputLines: List<String>): Int {
        validDigitToSymbolsMap = mutableMapOf()
        symbolToPartNumbersMap = mutableMapOf()
        return solve(inputLines, 1)
    }

    override fun solvePartTwo(inputLines: List<String>): Int {
        validDigitToSymbolsMap = mutableMapOf()
        symbolToPartNumbersMap = mutableMapOf()
        return solve(inputLines, 2)
    }

    private fun solve(inputLines: List<String>, part: Int): Int {
        val validNumbers = mutableListOf<Int>()

        for (row in inputLines.indices) {
            val line = inputLines[row]
            for (column in line.indices) {
                val char = line[column]
                if (isSymbol(char)) {
                    markSymbol(row, column, line.length, inputLines.size)
                }
            }
        }

        for (row in inputLines.indices) {
            val line = inputLines[row]

            var currentNumber = ""
            var valid = false
            var affectingSymbols = mutableSetOf<Pair<Int, Int>>()
            for (column in line.indices) {
                val char = line[column]
                if (char.isDigit()) {
                    currentNumber += char
                    if (validDigitToSymbolsMap[row to column] != null) {
                        valid = true
                        validDigitToSymbolsMap[row to column]?.forEach { affectingSymbols.add(it) }
                    }
                }

                // line.lastIndex is 10+ms slower - wild
                if (!char.isDigit() || column == line.length - 1) {
                    if (currentNumber.isNotEmpty() && valid) {
                        val number = currentNumber.toInt()
                        validNumbers.add(number)
                        affectingSymbols.forEach {
                            if (symbolToPartNumbersMap[it] == null) symbolToPartNumbersMap[it] = mutableListOf(number)
                            else symbolToPartNumbersMap[it]?.add(number)
                        }
                    }
                    valid = false
                    currentNumber = ""
                    affectingSymbols = mutableSetOf()
                }
            }
        }

        var sum = 0
        if (part == 1) {
            validNumbers.forEach { sum += it }
        } else {
            symbolToPartNumbersMap.values.forEach {
                var gearRatio = 0
                if (it.size > 1) {
                    it.forEach { gearNumber ->
                        gearRatio = if (gearRatio == 0) gearNumber else gearNumber * gearRatio
                    }
                }
                sum += gearRatio
            }
        }

        return sum
    }

    private fun isSymbol(c: Char): Boolean {
        return (!c.isDigit() && c != '.')
    }

    private fun markSymbol(row: Int, column: Int, rowMax: Int, columnMax: Int) {
        val prevRow = row - 1
        val nextRow = row + 1
        if (prevRow >= 0) markColumn(row, column, prevRow, column, columnMax)
        markColumn(row, column, row, column, columnMax)
        if (nextRow <= rowMax) markColumn(row, column, nextRow, column, columnMax)
    }

    private fun markColumn(symbolRow: Int, symbolColumn: Int, row: Int, column: Int, columnMax: Int) {
        val prevColumn = column - 1
        val nextColumn = column + 1
        if (prevColumn >= 0) addValidDigit(symbolRow, symbolColumn, row, prevColumn)
        addValidDigit(symbolRow, symbolColumn, row, column)
        if (nextColumn <= columnMax) addValidDigit(symbolRow, symbolColumn, row, nextColumn)
    }

    private fun addValidDigit(symbolRow: Int, symbolColumn: Int, row: Int, column: Int) {
        val list = validDigitToSymbolsMap[row to column]
        if (list == null) {
            validDigitToSymbolsMap[row to column] = mutableListOf(symbolRow to symbolColumn)
        } else {
            list.add(symbolRow to symbolColumn)
        }
    }
}