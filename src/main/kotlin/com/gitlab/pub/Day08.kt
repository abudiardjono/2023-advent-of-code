package com.gitlab.pub

fun main(args: Array<String>) {
    Day08("day08.txt").solveAndPrint()
}

/**
 * This one is easy if you assume that every time you run out of instructions you MUST run through the full
 * instruction set again in order to get to the target. (spoiler alert I did not assume that at first).
 *
 * Part 1 is extra easy because you can just follow the map until you hit ZZZ from AAA.
 *
 * If you make the aforementioned assumption, for Part 2 you can just solve each starting node separately then find
 * the least common multiple of all their final number of steps.
 * I overcomplicated it until I finally said, whatever ima just assume this.
 */
class Day08(input: String) : Day(input) {

    private val mapRegex = "(?<src>\\w+) = \\((?<destL>\\w+), (?<destR>\\w+)\\)".toRegex()
    private val nodeMap = mutableMapOf<String, Pair<String, String>>()
    private val workingNodes = mutableListOf<String>()
    private val directionStepMap = mutableMapOf<String, Pair<String, Int>>()

    override fun solvePartOne(inputLines: List<String>): Number {
        return solve(inputLines, 1)
    }

    override fun solvePartTwo(inputLines: List<String>): Number {
        return solve(inputLines, 2)
    }

    private fun solve(inputLines: List<String>, part: Int): Number {
        lateinit var directions: String
        inputLines.forEach { line ->
            if (inputLines.indexOf(line) == 0) directions = line
            if (mapRegex.matches(line)) {
                val result = mapRegex.find(line)!!
                val src = result.groups["src"]?.value!!
                val destL = result.groups["destL"]?.value!!
                val destR = result.groups["destR"]?.value!!
                nodeMap[src] = destL to destR

                if (part == 1 && src == "AAA") workingNodes.add(src)
                else if (part == 2 && src[2] == 'A') workingNodes.add(src)
            }
        }

        // map each node to the node they end up at after finishing a single direction set
        // could potentially make it slower instead of faster /shrug/
        for (node in nodeMap.keys) {
            var steps = 0
            var currNode = node
            directions.forEach { direction ->
                val currDirections = nodeMap[currNode]!!
                currNode = if (direction == 'L') currDirections.first else currDirections.second
                steps++
                if (currNode[2] == 'Z') {
                    return@forEach
                }
            }
            directionStepMap[node] = currNode to steps
        }

        val solutionsList = mutableListOf<Int>()
        for (node in workingNodes) {
            var currNode = node
            var currCount = 0
            while (currNode[2] != 'Z') {
                val nextNodeEntry = directionStepMap[currNode]!!
                currNode = nextNodeEntry.first
                currCount += nextNodeEntry.second
            }
            solutionsList.add(currCount)
        }

        var solution = -1L
        if (part == 1) solution = solutionsList[0].toLong()
        else {
            // find the lcm of all solutions
            solutionsList.forEach { sol ->
                solution = if (solution == -1L) sol.toLong()
                else {
                    lcm(solution, sol.toLong())
                }
            }
        }

        return solution
    }

    private fun lcm(a: Long, b: Long): Long {
        val lcmStep = a.coerceAtLeast(b)
        val maxValue = a * b
        var lcm = lcmStep
        while (lcm <= maxValue) {
            if (lcm % a == 0L && lcm % b == 0L) {
                return lcm
            }
            lcm += lcmStep
        }
        return maxValue
    }
}