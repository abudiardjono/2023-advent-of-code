package com.gitlab.pub

fun main(args: Array<String>) {
    DayFour("day04.txt").solveAndPrint()
}

/**
 * We're having fun and using regex this time around. This can be slower, but it's probably more practical. For
 * Part 1 we're being extra lazy and just checking if our numbers are in the list of winning numbers with contains().
 *
 * For Part 2, we know a couple of things.
 * 1. for any card 1..n, num(n) = 1 + num(n-1) + num(n-2)...+ num(1)
 * 2. num(1) = 1 since there are no previous cards to spawn copies
 * 3. for any card 1..n, it has a number of m matches
 * 4. for any card 1..n, num(1) >= 1 since there is at least 1 original
 * 5. for any card 1..n, it adds num(n) copies to all cards in the range (n+1, n+m)
 *
 * Starting from card 1, at any point n, we know num(n), because it's always 1 + num(n-1).
 * Since we also know m, we know that for the next (n+1, n+m) cards we will spawn m extra copies of each
 */
class DayFour(input1: String) : Day(input1) {
    private val lineRegex =
        "Card\\s+(?<cardNumber>\\d+):\\s+(?<winningNumbers>[\\d\\s]+)\\s+\\|\\s+(?<numbers>[\\d\\s]+)".toRegex()
    private val cardCountMap = mutableMapOf<Int, Int>() // an int array will take less space

    override fun solvePartOne(inputLines: List<String>): Int {
        var sum = 0
        for (index in inputLines.indices) sum += cardScore(index, inputLines, 1)
        return sum
    }

    override fun solvePartTwo(inputLines: List<String>): Int {
        for (index in inputLines.indices) {
            val score = cardScore(index, inputLines, 2)
            if (cardCountMap[index] == null) cardCountMap[index] = 1
            for (i in 1..score) {
                val mapIndex = index + i
                if (mapIndex <= inputLines.lastIndex) {
                    val cardCount = cardCountMap[mapIndex]
                    if (cardCount == null) cardCountMap[mapIndex] = 1 + cardCountMap[index]!!
                    else cardCountMap[mapIndex] = cardCount + cardCountMap[index]!!
                }
            }
        }
        return cardCountMap.values.sum()
    }

    private fun cardScore(card: Int, cardList: List<String>, part: Int): Int {
        var cardScore = 0
        // doing something different this time, regex can be slow but solution is interesting
        lineRegex.find(cardList[card])?.let { result ->
            val winningNumbers =
                result.groups["winningNumbers"]?.value?.split("\\s+".toRegex())?.filter { it.isNotEmpty() }
            val numbers = result.groups["numbers"]?.value?.split("\\s+".toRegex())?.filter { it.isNotEmpty() }
            numbers?.forEach {
                if (winningNumbers?.contains(it)!!) {
                    if (part == 1) cardScore = if (cardScore == 0) 1 else cardScore * 2
                    else cardScore++
                }
            }
        }
        return cardScore
    }
}